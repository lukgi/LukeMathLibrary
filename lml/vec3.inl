#pragma once
namespace lml {
    // Constructors
    template<typename T>
    Vector<T, 3>::Vector() : data{} {
    }

    template<typename T>
    Vector<T, 3>::Vector(const std::array<T, 3> data) : data(data) {
    }

    template<typename T>
    Vector<T, 3>::Vector(const T f)
    {
        data.fill(f);
    }

    template<typename T>
    Vector<T, 3>::Vector(T x, T y, T z) : data{ x, y, z } {
    }

    template<typename T>
    Vector<T, 3>::Vector(const Vector& rhs) : data(rhs.data)
    {
    }

    template<typename T>
    Vector<T, 3>::Vector(Vector&& rhs)
    {
        swap(*this, rhs);
    }

    // Operator overloads
    // Const
    template<typename T>
    const T& Vector<T, 3>::operator[](std::size_t id) const
    {
        return data[id];
    }

    // Non-const
    template<typename T>
    T& Vector<T, 3>::operator[](std::size_t id)
    {
        return data[id];
    }

    template<typename T>
    Vector<T, 3>& Vector<T, 3>::operator=(Vector rhs)
    {
        swap(*this, rhs);
        return *this;
    }

    template<typename T>
    Vector<T, 3>& Vector<T, 3>::operator=(Vector&& rhs)
    {
        swap(*this, rhs);
        return *this;
    }
};