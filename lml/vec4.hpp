#pragma once
namespace lml {
    // Vector 4
    template <typename T> struct Vector<T, 4>
    {
        union
        {
            std::array<T, 4> data;
            struct { T x, y, z, w; };
            Vector<T, 3> xyz;
        };

        // Constructors
        explicit Vector() : data{} {
        }

        explicit Vector(const std::array<T, 4> data) : data(data){
        }

        explicit Vector(const T f)
        {
            data.fill(f);
        }

        explicit Vector(T x, T y, T z, T w) : data{ x, y, z, w } {
        }

        Vector(const Vector& rhs) : data(rhs.data)
        {
        }

        explicit Vector(Vector&& rhs)
        {
            swap(*this, rhs);
        }

        // Operator overloads
        // Const
        const T& operator[](std::size_t id) const
        {
            return data[id];
        }

        // Non-const
        T& operator[](std::size_t id)
        {
            return data[id];
        }

        Vector& operator=(Vector rhs)
        {
            swap(*this, rhs);
            return *this;
        }

        Vector& operator=(Vector&& rhs)
        {
            swap(*this, rhs);
            return *this;
        }
    };

    typedef Vector<float, 4> Vec4;
};