#pragma once
namespace lml {
    template <typename T>
    struct Matrix<T, 2, 2>
    {
        typedef Vector<T, 2> colType;
        std::array<colType, 2> columns;

        explicit Matrix();

        explicit Matrix(const std::array<colType, 3>& columns);

        explicit Matrix(const colType& v1, const colType& v2);

        Matrix(const Matrix& matrix);

        explicit Matrix(const Matrix&& matrix);

        // Operator overloads
        // Const
        const colType& operator[](std::size_t id) const;

        // Non-const
        colType& operator[](std::size_t id);

        Matrix& operator=(Matrix matrix);

        Matrix& operator=(const Matrix&& matrix);
    };

    typedef Matrix<float, 2, 2> Mat2x2;
}

#include "mat2x2.inl"