#pragma once
namespace lml {
    // Functions
    template <typename T, int n>
    float dot(const Vector<T, n>& lhs, const Vector<T, n>& rhs)
    {
        float sum(0);
        for (auto i = 0; i < n; i++)
        {
            sum += lhs[i] * rhs[i];
        }

        return sum;
    }

    template <typename T, int n>
    float length(const Vector<T, n>& Vector)
    {
        return std::sqrt(squaredLength(Vector));
    }

    template <typename T, int n>
    float squaredLength(const Vector<T, n>& Vector)
    {
        float sum(0);
        for (auto i = 0; i < n; i++)
        {
            sum += Vector[i] * Vector[i];
        }

        return sum;
    }

    template <typename T, int n>
    Vector<T, n> normalize(const Vector<T, n>& Vector)
    {
        return Vector / length(Vector);
    }

    template <typename T, int n>
    float distance(const Vector<T, n>& p0, const Vector<T, n>& p1)
    {
        return length(p0 - p1);
    }

    // I is the incident Vector, N is normalized normal.
    template <typename T, int n>
    Vector<T, n> reflect(const Vector<T, n>& incidentVecI, const Vector<T, n>& normalizedVecN)
    {
        return incidentVecI - (2 * (dot(incidentVecI, normalizedVecN)))*normalizedVecN;
    }

    template <typename T, int n>
    Vector<T, n> refract(const Vector<T, n>& normalizedIncidentVecI, const Vector<T, n>& normalizedVecN, float eta)
    {
        float nDotI = dot(normalizedIncidentVecI, normalizedVecN);
        float k = 1 - eta*eta*(1 - nDotI*nDotI);
        if (k < 0.0f)
        {
            return Vector<T, n>(0);
        }
        else
        {
            return eta*normalizedIncidentVecI - (eta*nDotI + std::sqrt(k))*normalizedVecN;
        }
    }

    template<typename T>
    Vector<T, 3> cross(const Vector<T, 3>&  lhs, const Vector<T, 3> & rhs)
    {
        return Vector<T, 3>(lhs[1] * rhs[2] - lhs[2] * rhs[1],
            lhs[2] * rhs[0] - lhs[0] * rhs[2],
            lhs[0] * rhs[1] - lhs[1] * rhs[0]);
    }
};