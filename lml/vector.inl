#pragma once
namespace lml
{
    template <typename T, int n>
    Vector<T, n>::Vector() : data{} {
    }

    template <typename T, int n>
    Vector<T, n>::Vector(const std::array<T, n> data) : data(data)
    {
    }

    template <typename T, int n>
    Vector<T, n>::Vector(const T f)
    {
        data.fill(f);
    }

    template <typename T, int n>
    Vector<T, n>::Vector(const Vector<T, n>& rhs) : data(rhs.data)
    {
    }

    template <typename T, int n>
    Vector<T, n>::Vector(Vector<T, n>&& rhs)
    {
        swap(*this, rhs);
    }

    // Operator overloads
    // Const
    template <typename T, int n>
    const T& Vector<T, n>::operator[](std::size_t id) const
    {
        return data[id];
    }

    // Non-const
    template <typename T, int n>
    T& Vector<T, n>::operator[](std::size_t id)
    {
        return data[id];
    }

    template <typename T, int n>
    Vector<T, n>& Vector<T, n>::operator=(Vector<T, n> rhs)
    {
        swap(*this, rhs);
        return *this;
    }

    template <typename T, int n>
    Vector<T, n>& Vector<T, n>::operator=(Vector<T, n>&& rhs)
    {
        swap(*this, rhs);
        return *this;
    }

    // Operator overloads
    // Const
    template <typename T, typename S, int n>
    bool operator==(const Vector<T, n>& lhs, const Vector<S, n>& rhs) {
        return squaredLength(lhs - rhs) < Constants::epsilon;
    }

    template <typename T, typename S, int n>
    bool operator!=(const Vector<T, n>& lhs, const Vector<S, n>& rhs) {
        return !(lhs == rhs);
    }

    template <typename T, typename S, int n>
    Vector<T, n> operator+(const Vector<T, n>& lhs, const Vector<S, n>& rhs) {
        auto result(lhs);
        return result += rhs;
    }

    template <typename T, typename S, int n>
    Vector<T, n> operator-(const Vector<T, n>& lhs, const Vector<S, n>& rhs) {
        auto result(lhs);
        return result -= rhs;
    }

    template <typename T, typename S, int n>
    Vector<T, n> operator*(const Vector<T, n>& lhs, const S rhs) {
        auto result(lhs);
        return result *= rhs;
    }

    template <typename T, typename S, int n>
    Vector<T, n> operator*(const S lhs, const Vector<T, n>& rhs) {
        return rhs*lhs;
    }

    template <typename T, typename S, int n>
    Vector<T, n> operator/(const Vector<T, n>& lhs, const S rhs) {
        auto result(lhs);
        return result /= rhs;
    }

    // Non-const
    template <typename T, typename S, int n>
    Vector<T, n>& operator+=(Vector<T, n>& lhs, const Vector<S, n>& rhs)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] += rhs[i];
        }

        return lhs;
    }

    template <typename T, typename S, int n>
    Vector<T, n>& operator-=(Vector<T, n>& lhs, const Vector<S, n>& rhs)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] -= rhs[i];
        }

        return lhs;
    }

    template <typename T, typename S, int n>
    Vector<T, n>& operator*=(Vector<T, n>& lhs, S f)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] *= f;
        }

        return lhs;
    }

    template <typename T, typename S, int n>
    Vector<T, n>& operator/=(Vector<T, n>& lhs, S f)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] /= f;
        }

        return lhs;
    }

    template <typename T, int n>
    std::ostream& operator<<(std::ostream& stream, const Vector<T, n>& rhs) {
        stream << "(";
        for (auto i = 0; i < n; i++)
        {
            stream << rhs[i];
            if (i < n - 1)
            {
                stream << ", ";
            }
        }

        stream << ")";
        return stream;
    }

    template <typename T, int n>
    std::wstringstream& operator<<(std::wstringstream& stream, const Vector<T, n>& rhs) {
        stream << "(";
        for (auto i = 0; i < n; i++)
        {
            stream << rhs[i];
            if (i < n - 1)
            {
                stream << ", ";
            }
        }

        stream << ")";
        return stream;
    }



    template <int n>
    bool operator==(const Vector<int, n>& lhs, const Vector<int, n>& rhs) {
        for (auto i = 0; i < n; i++)
        {
            if (lhs[i] != rhs[i]) {
                return false;
            }
        }

        return true;
    }


    template <typename T, int n>
    void swap(Vector<T, n>& lhs, Vector<T, n>& rhs)
    {
        using std::swap;
        swap(lhs.data, rhs.data);
    }
}