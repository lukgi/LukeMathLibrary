#pragma once
#include "constants.h"
#include <array>
#include <string>
namespace lml
{
    template <typename T, int n>
    struct Vector
    {
        std::array<T, n> data;

        // Constructors
        explicit Vector();
        explicit Vector(const std::array<T, n> data);
        explicit Vector(const T f);
        Vector(const Vector& rhs);
        explicit Vector(Vector&& rhs);
       
        // Operator overloads
        // Const
        const T& operator[](std::size_t id) const;

        // Non-const
        T& operator[](std::size_t id);

        Vector& operator=(Vector rhs);

        Vector& operator=(Vector&& rhs);
    };


    // Operator overloads
    // Const
    template <typename T, typename S, int n>
    bool operator==(const Vector<T, n>& lhs, const Vector<S, n>& rhs);

    template <typename T, typename S, int n>
    bool operator!=(const Vector<T, n>& lhs, const Vector<S, n>& rhs);

    template <typename T, typename S, int n>
    Vector<T, n> operator+(const Vector<T, n>& lhs, const Vector<S, n>& rhs);

    template <typename T, typename S, int n>
    Vector<T, n> operator-(const Vector<T, n>& lhs, const Vector<S, n>& rhs);

    template <typename T, typename S, int n>
    Vector<T, n> operator*(const Vector<T, n>& lhs, const S rhs);

    template <typename T, typename S, int n>
    Vector<T, n> operator*(const S lhs, const Vector<T, n>& rhs);

    template <typename T, typename S, int n>
    Vector<T, n> operator/(const Vector<T, n>& lhs, const S rhs);

    // Non-const
    template <typename T, typename S, int n>
    Vector<T, n>& operator+=(Vector<T, n>& lhs, const Vector<S, n>& rhs);

    template <typename T, typename S, int n>
    Vector<T, n>& operator-=(Vector<T, n>& lhs, const Vector<S, n>& rhs);

    template <typename T, typename S, int n>
    Vector<T, n>& operator*=(Vector<T, n>& lhs, S f);

    template <typename T, typename S, int n>
    Vector<T, n>& operator/=(Vector<T, n>& lhs, S f);

    template <typename T, int n>
    std::ostream& operator<<(std::ostream& stream, const Vector<T, n>& rhs);

    template <typename T, int n>
    std::wstringstream& operator<<(std::wstringstream& stream, const Vector<T, n>& rhs);

    template <int n>
    bool operator==(const Vector<int, n>& lhs, const Vector<int, n>& rhs);

    template <typename T, int n>
    void swap(Vector<T, n>& lhs, Vector<T, n>& rhs);
}



#include "vector.inl"
#include "vec2.hpp"
#include "vec3.hpp"
#include "vec4.hpp"
#include "vectorFunctions.hpp"