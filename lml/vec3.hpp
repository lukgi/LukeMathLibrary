#pragma once
namespace lml {
    // Vector 3
    template <typename T> struct Vector<T, 3>
    {
        union
        {
            std::array<T, 3> data;
            struct { T x, y, z; };
            Vector<T, 2> xy;
        };

        // Constructors
        explicit Vector();
        explicit Vector(const std::array<T, 3> data);
        explicit Vector(const T f);
        explicit Vector(T x, T y, T z);
        Vector(const Vector& rhs);
        explicit Vector(Vector&& rhs);
        // Operator overloads
        // Const
        const T& operator[](std::size_t id) const;

        // Non-const
        T& operator[](std::size_t id);

        Vector& operator=(Vector rhs);

        Vector& operator=(Vector&& rhs);
    };

    template<typename T>
    Vector<T, 3> cross(const Vector<T, 3>&  lhs, const Vector<T, 3> & rhs);

    typedef Vector<float, 3> Vec3;
};


#include "vec3.inl"