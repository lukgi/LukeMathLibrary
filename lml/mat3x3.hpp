#pragma once
namespace lml {
    template <typename T>
    struct Matrix<T, 3, 3>
    {
        typedef Vector<T, 3> colType;
        std::array<colType, 3> columns;

        explicit Matrix();

        explicit Matrix(const std::array<colType, 3>& columns);

        explicit Matrix(const colType& v1, const colType& v2, const colType& v3);

        Matrix(const Matrix& matrix);

        explicit Matrix(const Matrix&& matrix);

        // Operator overloads
        // Const
        const colType& operator[](std::size_t id) const;

        // Non-const
        colType& operator[](std::size_t id);

        Matrix& operator=(Matrix matrix);

        Matrix& operator=(const Matrix&& matrix);
    };

    typedef Matrix<float, 3, 3> Mat3x3;
}

#include "mat3x3.inl"