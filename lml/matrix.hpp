#pragma once
#include "vector.hpp"
#include <array>
#include <iomanip>
#include "constants.h"
#include <string>

namespace lml {
    template <typename T, int m, int n>
    struct Matrix
    {

        typedef Vector<T, m> colType;
        std::array<colType, n> columns;
      
        explicit Matrix();
        explicit Matrix(const std::array<colType, n>& columns);
        Matrix(const Matrix& matrix);
        explicit Matrix(const Matrix&& matrix);

        // Operator overloads
        // Const
        const colType& operator[](std::size_t id) const;
        // Non-const
        colType& operator[](std::size_t id);

        Matrix& operator=(Matrix matrix);

        Matrix& operator=(const Matrix&& matrix);
    };

    // Operator overloads
    // Const
    template <typename T, typename S, int m, int n>
    bool operator==(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs);

    template <typename T, typename S, int m, int n>
    bool operator!=(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator+(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator-(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator*(const Matrix<T, m, n>& lhs, const S rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator*(const S lhs, const Matrix<T, m, n>& rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator/(const Matrix<T, m, n>& lhs, const S rhs);

    template <typename T, int m, int n>
    std::ostream& operator<<(std::ostream& stream, const Matrix<T, m, n>& rhs);

    template <typename T, int m, int n>
    std::wstringstream& operator<<(std::wstringstream& stream, const Matrix<T, m, n>& rhs);

    // Non-const
    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator+=(Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator-=(Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs);

    template <typename T, int m, int n, int p>
    Matrix<T, m, p> operator*(const Matrix<T, m, n>& lhs, const Matrix<T, n, p>& rhs);

    template <typename T, int m, int n>
    Vector<T, n> operator*(const Matrix<T, m, n>& lhs, const Vector<T, n>& rhs);

    template <typename T, int m, int n>
    Vector<T, m> operator*(const Vector<T, m>& lhs, const Matrix<T, m, n>& rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator*=(Matrix<T, m, n>& lhs, S rhs);

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator/=(Matrix<T, m, n>& lhs, S f);

    template <int m, int n>
    bool operator==(const Matrix<int, m, n>& lhs, const Matrix<int, m, n>& rhs);

    template <typename T, int m, int n>
    void swap(Matrix<T, m, n>& lhs, Matrix<T, m, n>& rhs);
}

#include"matrix.inl"
#include "mat2x2.hpp"
#include "mat3x3.hpp"
#include "mat4x4.hpp"
#include "matrixFunctions.hpp"