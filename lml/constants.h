#pragma once
namespace lml
{
    namespace Constants
    {
        constexpr float epsilon = 0.0001f;
    }
}