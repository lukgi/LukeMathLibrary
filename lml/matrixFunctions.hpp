namespace lml {
    namespace internal
    {
        template<typename T>
        float determinant(const Matrix<T, 2, 2> m)
        {

            return (m[0][0] * m[1][1] - m[0][1] * m[1][0]);
        }

        template<typename T>
        float determinant(const Matrix<T, 3, 3> m)
        {
            return  m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2]) +
                    m[1][0] * (m[2][0] * m[0][2] - m[0][1] * m[2][1]) +
                    m[2][0] * (m[0][1] * m[1][2] - m[1][1] * m[0][2]);
        }

        template<typename T>
        float determinant(const Matrix<T, 4, 4> m)
        {
            Vec3 subVec1(m[0][1], m[0][2], m[0][3]);
            Vec3 subVec2(m[1][1], m[1][2], m[1][3]);
            Vec3 subVec3(m[2][1], m[2][2], m[2][3]);
            Vec3 subVec4(m[3][1], m[3][2], m[3][3]);

            return m[0][0] * determinant(Mat3x3(subVec2, subVec3, subVec4)) -
                m[1][0] * determinant(Mat3x3(subVec1, subVec3, subVec4)) +
                m[2][0] * determinant(Mat3x3(subVec1, subVec2, subVec4)) -
                m[3][0] * determinant(Mat3x3(subVec1, subVec2, subVec4));
        }
    }

    template<typename T, int m, int n>
    float determinant(const Matrix<T, m, n> m)
    {
        return internal::determinant(m);
    }


}