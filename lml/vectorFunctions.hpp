#pragma once
namespace lml {
    // Functions
    template <typename T, int n>
    float dot(const Vector<T, n>& lhs, const Vector<T, n>& rhs);

    template <typename T, int n>
    float length(const Vector<T, n>& Vector);

    template <typename T, int n>
    float squaredLength(const Vector<T, n>& Vector);

    template <typename T, int n>
    Vector<T, n> normalize(const Vector<T, n>& Vector);

    template <typename T, int n>
    float distance(const Vector<T, n>& p0, const Vector<T, n>& p1);

    // I is the incident Vector, N is normalized normal.
    template <typename T, int n>
    Vector<T, n> reflect(const Vector<T, n>& incidentVecI, const Vector<T, n>& normalizedVecN);

    template <typename T, int n>
    Vector<T, n> refract(const Vector<T, n>& normalizedIncidentVecI, const Vector<T, n>& normalizedVecN, float eta);

    template<typename T>
    Vector<T, 3> cross(const Vector<T, 3>&  lhs, const Vector<T, 3> & rhs);
};

#include "vectorFunctions.inl"