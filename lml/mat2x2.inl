#pragma once
namespace lml {
    template<typename T>
    Matrix<T, 2, 2>::Matrix() {
        columns[0][0] = static_cast<T>(1);
        columns[1][1] = static_cast<T>(1);
    }

    template<typename T>
    Matrix<T, 2, 2>::Matrix(const std::array<colType, 3>& columns) : columns(columns)
    {
    }

    template<typename T>
    Matrix<T, 2, 2>::Matrix(const colType& v1, const colType& v2) : columns(std::array<colType, 3>{v1, v2})
    {

    }

    template<typename T>
    Matrix<T, 2, 2>::Matrix(const Matrix& matrix) : columns(matrix.columns)
    {
    }

    template<typename T>
    Matrix<T, 2, 2>::Matrix(const Matrix&& matrix)
    {
        swap(*this, matrix);
    }

    // Operator overloads
    // Const
    template<typename T>

    const typename Matrix<T, 2, 2>::colType& Matrix<T, 2, 2>::operator[](std::size_t id) const {
        return columns[id];
    }

    // Non-const
    template<typename T>
    typename Matrix<T, 2, 2>::colType& Matrix<T, 2, 2>::operator[](std::size_t id) {
        return columns[id];
    }

    template<typename T>
    Matrix<T, 2, 2>& Matrix<T, 2, 2>::operator=(Matrix matrix) {
        swap(*this, matrix);
        return *this;
    }

    template<typename T>
    Matrix<T, 2, 2>& Matrix<T, 2, 2>::operator=(const Matrix&& matrix) {
        swap(*this, matrix);
        return *this;
    }
}
