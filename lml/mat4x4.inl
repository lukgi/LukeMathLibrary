#pragma once
namespace lml {
    template<typename T>
        Matrix<T, 4, 4>::Matrix() : columns{}
        {
            columns[0][0] = static_cast<T>(1);
            columns[1][1] = static_cast<T>(1);
            columns[2][2] = static_cast<T>(1);
            columns[3][3] = static_cast<T>(1);
        }

        template<typename T>
        Matrix<T, 4, 4>::Matrix(const std::array<colType, 3>& columns) : columns(columns)
        {
        }


        template<typename T>
        Matrix<T, 4, 4>::Matrix(const colType& v1, const colType& v2, const colType& v3, const colType& v4) : columns(std::array<colType, 3>{v1, v2, v3, v4})
        {
        }

        template<typename T>
        Matrix<T, 4, 4>::Matrix(const Matrix& matrix) : columns(matrix.columns)
        {
        }

        template<typename T>
        Matrix<T, 4, 4>::Matrix(const Matrix&& matrix)
        {
            swap(*this, matrix);
        }

        // Operator overloads
        // Const
        template<typename T>
        const typename Matrix<T, 4, 4>::colType& Matrix<T, 4, 4>::operator[](std::size_t id) const
        {
            return columns[id];
        }

        // Non-const
        template<typename T>
        typename Matrix<T, 4, 4>::colType& Matrix<T, 4, 4>::operator[](std::size_t id)
        {
            return columns[id];
        }

        template<typename T>
        Matrix<T, 4, 4>& Matrix<T, 4, 4>::operator=(Matrix matrix) {
            swap(*this, matrix);
            return *this;
        }

        template<typename T>
        Matrix<T, 4, 4>& Matrix<T, 4, 4>::operator=(const Matrix&& matrix) {
            swap(*this, matrix);
            return *this;
        }
}