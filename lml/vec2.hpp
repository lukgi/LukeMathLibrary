#pragma once
namespace lml {
    // Vector 2
    template <typename T> struct Vector<T, 2>
    {
        union
        {
            std::array<T, 2> data;
            struct { T x; T y; };
        };

        // Constructors
        explicit Vector();
        explicit Vector(const std::array<T, 2> data);
        explicit Vector(const T f);
        explicit Vector(T x, T y);
        Vector(const Vector& rhs);
        explicit Vector(Vector&& rhs);
            // Operator overloads
        // Const
        const T& operator[](std::size_t id) const;

        // Non-const
        T& operator[](std::size_t id);

        Vector& operator=(Vector rhs);

        Vector& operator=(Vector&& rhs);
    };

    typedef Vector<float, 2> Vec2;

};

#include "vec2.inl"