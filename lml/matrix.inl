#pragma once
namespace lml {

    template<typename T, int m, int n>
    Matrix<T, m, n>::Matrix() : columns{}
    {
    }

    template<typename T, int m, int n>
    Matrix<T, m, n>::Matrix(const std::array<colType, n>& columns) : columns(columns)
    {
    }

    template<typename T, int m, int n>
    Matrix<T, m, n>::Matrix(const Matrix& matrix) : columns(matrix.columns)
    {
    }

    template<typename T, int m, int n>
    Matrix<T, m, n>::Matrix(const Matrix&& matrix)
    {
        swap(*this, matrix);
    }

    // Operator overloads
    // Const
    template<typename T, int m, int n>
    const typename Matrix<T, m, n>::colType& Matrix<T, m, n>::operator[](std::size_t id) const
    {
        return columns[id];
    }

    // Non-const
    template<typename T, int m, int n>
    typename Matrix<T, m, n>::colType& Matrix<T, m, n>::operator[](std::size_t id)
    {
        return columns[id];
    }

    template<typename T, int m, int n>
    Matrix<T, m, n>& Matrix<T, m, n>::operator=(Matrix matrix) {
        swap(*this, matrix);
        return *this;
    }

    template<typename T, int m, int n>
    Matrix<T, m, n>& Matrix<T, m, n>::operator=(const Matrix&& matrix) {
        swap(*this, matrix);
        return *this;
    }

    // Operator overloads
    // Const
    template <typename T, typename S, int m, int n>
    bool operator==(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs) {
        for (auto i = 0; i < n; i++)
        {
            if (squaredLength(lhs[i] - rhs[i]) > lml::Constants::epsilon)
            {
                return false;
            }
        }

        return true;
    }

    template <typename T, typename S, int m, int n>
    bool operator!=(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs) {
        return !(lhs == rhs);
    }


    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator+(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs) {
        auto result(lhs);
        return result += rhs;
    }

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator-(const Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs) {
        auto result(lhs);
        return result -= rhs;
    }

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator*(const Matrix<T, m, n>& lhs, const S rhs) {
        auto result = Matrix<T, m, n>(lhs);
        return result *= rhs;
    }

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator*(const S lhs, const Matrix<T, m, n>& rhs) {
        return rhs*lhs;
    }

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n> operator/(const Matrix<T, m, n>& lhs, const S rhs) {
        auto result(lhs);
        return result /= rhs;
    }

    template <typename T, int m, int n>
    std::ostream& operator<<(std::ostream& stream, const Matrix<T, m, n>& rhs) {
        stream << "(";
        for (auto i = 0; i < n; i++)
        {
            for (auto j = 0; j < m; j++)
            {
                stream << std::setprecision(3) << std::fixed << rhs[i][j];
                if (i != 0 && i % (n - 1) == 0)
                {
                    stream << "\n";
                }
                else if (i*j < n*m - 1)
                {
                    stream << " ";
                }
            }
        }

        return stream;
    }

    template <typename T, int m, int n>
    std::wstringstream& operator<<(std::wstringstream& stream, const Matrix<T, m, n>& rhs) {
        for (auto i = 0; i < n; i++)
        {
            for (auto j = 0; j < m; j++)
            {
                stream << std::setprecision(3) << std::fixed << rhs[i][j];
                if (j < n - 1)
                {
                    stream << " ";
                }
            }
            if (i < m - 1)
            {
                stream << "\n";
            }
        }

        return stream;
    }

    // Non-const
    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator+=(Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] += rhs[i];
        }

        return lhs;
    }

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator-=(Matrix<T, m, n>& lhs, const Matrix<S, m, n>& rhs)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] -= rhs[i];
        }

        return lhs;
    }

    template <typename T, int m, int n, int p>
    Matrix<T, m, p> operator*(const Matrix<T, m, n>& lhs, const Matrix<T, n, p>& rhs) {
        Matrix<T, m, p> result;
        for (auto i = 0; i < m; i++)
        {
            for (auto j = 0; j < p; j++)
            {
                for (auto k = 0; k < n; k++)
                {
                    result[j][i] += lhs[k][i] * rhs[j][k];
                }
            }

        }

        return result;
    }

    template <typename T, int m, int n>
    Vector<T, n> operator*(const Matrix<T, m, n>& lhs, const Vector<T, n>& rhs) {
        Vector<T, n> result(0);
        for (auto i = 0; i < m; i++)
        {
            for (auto j = 0; j < n; j++)
            {
                result[i] += lhs[i][j] + rhs[j];
            }

        }

        return result;

    }

    template <typename T, int m, int n>
    Vector<T, m> operator*(const Vector<T, m>& lhs, const Matrix<T, m, n>& rhs) {
        Vector<T, m> result(0);
        for (auto i = 0; i < n; i++)
        {
            for (auto j = 0; j < m; j++)
            {
                result[i] += lhs[j] + rhs[i][j];
            }

        }

        return result;

    }

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator*=(Matrix<T, m, n>& lhs, S rhs)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] *= rhs;
        }

        return lhs;
    }

    template <typename T, typename S, int m, int n>
    Matrix<T, m, n>& operator/=(Matrix<T, m, n>& lhs, S f)
    {
        for (auto i = 0; i < n; i++)
        {
            lhs[i] /= f;
        }

        return lhs;
    }

    template <int m, int n>
    bool operator==(const Matrix<int, m, n>& lhs, const Matrix<int, m, n>& rhs) {
        for (auto i = 0; i < n; i++)
        {
            if (lhs[i] != rhs[i]) {
                return false;
            }
        }

        return true;
    }

    template <typename T, int m, int n>
    void swap(Matrix<T, m, n>& lhs, Matrix<T, m, n>& rhs)
    {
        using std::swap;
        swap(lhs.columns, rhs.columns);
    }
}