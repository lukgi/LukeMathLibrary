#pragma once
namespace lml {

    template<typename T>
    Matrix<T, 3, 3>::Matrix() : columns{}
    {
        columns[0][0] = static_cast<T>(1);
        columns[1][1] = static_cast<T>(1);
        columns[2][2] = static_cast<T>(1);
    }

    template<typename T>
    Matrix<T, 3, 3>::Matrix(const std::array<colType, 3>& columns) : columns(columns)
    {
    }

    template<typename T>
    Matrix<T, 3, 3>::Matrix(const colType& v1, const colType& v2, const colType& v3) : columns(std::array<colType, 3>{v1, v2, v3})
    {
    }

    template<typename T>
    Matrix<T, 3, 3>::Matrix(const Matrix& matrix) : columns(matrix.columns)
    {
    }

    template<typename T>
    Matrix<T, 3, 3>::Matrix(const Matrix&& matrix)
    {
        swap(*this, matrix);
    }

    // Operator overloads
    // Const
    template<typename T>
    const typename Matrix<T, 3, 3>::colType& Matrix<T, 3, 3>::operator[](std::size_t id) const
    {
        return columns[id];
    }

    // Non-const
    template<typename T>
    typename Matrix<T, 3, 3>::colType& Matrix<T, 3, 3>::operator[](std::size_t id)
    {
        return columns[id];
    }


    template<typename T>
    Matrix<T, 3, 3>& Matrix<T, 3, 3>::operator=(Matrix matrix) {
        swap(*this, matrix);
        return *this;
    }

    template<typename T>
    Matrix<T, 3, 3>& Matrix<T, 3, 3>::operator=(const Matrix&& matrix) {
        swap(*this, matrix);
        return *this;
    }
}