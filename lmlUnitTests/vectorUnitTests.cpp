#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\lml\vector.hpp"
#include "../lml/vectorFunctions.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft
{
    namespace VisualStudio
    {
        namespace CppUnitTestFramework
        {
            template<> inline std::wstring ToString<lml::Vec3>(const lml::Vec3& t) { RETURN_WIDE_STRING(t); }
        }
    }
}
namespace lmlUnitTests
{		
	TEST_CLASS(VectorTests)
	{
	public:

		TEST_METHOD(BinaryOperators)
		{
            const lml::Vec3 vec1(1.0f, 20.0f, 300.0f);
            const auto vec2 = lml::Vec3(3.0f, 30.0f, 300.0f);
            const auto vec3 = vec1;

            Assert::AreEqual(vec3, vec1);
            Assert::AreNotEqual(vec3 == vec1, vec3 != vec1);
            Assert::AreEqual(lml::Vec3(4.0f, 50.0f, 600.0f), vec1 + vec2);
            Assert::AreEqual(lml::Vec3(-2.0f, -10.0f, 0.0f), vec1 - vec2);
            Assert::AreEqual(lml::Vec3(3.0f, 60.0f, 900.0f), (vec1 * 3));
            Assert::AreEqual(lml::Vec3(3.0f, 60.0f, 900.0f), (3*vec1));
            Assert::AreEqual(lml::Vec3(1.0f, 10.0f, 100.0f), vec2 / 3);
		}

		TEST_METHOD(UnaryOperators)
		{
            const auto vec1 = lml::Vec3(1.0f, 20.0f, 300.0f);
            const auto vec2 = lml::Vec3(3.0f, 30.0f, 300.0f);
            auto vec3 = vec1;
            vec3 += vec2;
            Assert::AreEqual(lml::Vec3(4.0f, 50.0f, 600.0f), vec3);

            vec3 = vec1;
            vec3 -= vec2;
            Assert::AreEqual(lml::Vec3(-2.0f, -10.0f, 0.0f), vec3);

            vec3 = vec1;
            vec3 *= 3;
            Assert::AreEqual(lml::Vec3(3.0f, 60.0f, 900.0f), vec3);

            vec3 = vec2;
            vec3 /= 3;
            Assert::AreEqual(lml::Vec3(1.0f, 10.0f, 100.0f), vec3);
		}

        TEST_METHOD(Functions)
		{
            const lml::Vec3 vec1(3, -1, 1);
            const lml::Vec3 vec2(4, 9, 2);
            auto crossProd = lml::cross(vec1, vec2);
            auto dotProd = lml::dot(vec1, vec2);
            auto length = lml::length(vec2);
            auto squaredLength = lml::squaredLength(vec2);
            auto normalizedVector = lml::normalize(vec2);
            auto distance = lml::distance(vec1, vec2);

            Assert::AreEqual(lml::Vec3(-11, -2, 31), crossProd);
            Assert::AreEqual(5.0f, dotProd, 0.01f);
            Assert::AreEqual(101.0f, squaredLength, 0.01f);
            Assert::AreEqual(10.0498f, length, 0.01f);
            Assert::AreEqual(lml::Vec3(static_cast<float>(4/sqrt(101)), static_cast<float>(9/sqrt(101)), static_cast<float>(2/(sqrt(101)))),  normalizedVector);
            Assert::AreEqual(10.0995f, distance, 0.01f);

            const auto vecI = lml::normalize(lml::Vec3(-4, 3, 2));
            const auto vecN = lml::normalize(lml::Vec3(-7, -1, -3));
            Assert::AreEqual(lml::Vec3(0.09442135f, 0.67668640f, 0.730191838f), lml::reflect(vecI, vecN));
            Assert::AreEqual(lml::Vec3(0.65441972f, 0.42508735f, 0.625328367f), lml::refract(vecI, vecN, 0.5));
            Assert::AreEqual(lml::Vec3(0.0f, 0.0f, 0.0f), lml::refract(vecI, vecN, 2));
		}

	};
}