#include "stdafx.h"
#include <array>
#include "CppUnitTest.h"
#include "..\lml\matrix.hpp"
#include "../lml/vectorFunctions.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft
{
    namespace VisualStudio
    {
        namespace CppUnitTestFramework
        {
            template<> inline std::wstring ToString<lml::Matrix<float, 2, 3>>(const lml::Matrix<float, 2, 3>& t) { RETURN_WIDE_STRING(t); }
            template<> inline std::wstring ToString<lml::Matrix<float, 3, 2>>(const lml::Matrix<float, 3, 2>& t) { RETURN_WIDE_STRING(t); }
            template<> inline std::wstring ToString<lml::Matrix<float, 3, 3>>(const lml::Matrix<float, 3, 3>& t) { RETURN_WIDE_STRING(t); }
        }
    }
}
namespace lmlUnitTests
{
    TEST_CLASS(MatrixTests)
    {
    public:

        typedef lml::Matrix<float, 2, 3> Mat23;
        typedef lml::Matrix<float, 3, 2> Mat32;

        TEST_METHOD(BinaryOperators)
        {
            const Mat23 mat23_1(std::array<lml::Vec2, 3> {lml::Vec2(1.0f, 2.0f), lml::Vec2(3.0f, 4.0f), lml::Vec2(5.0f, 6.0f)});
            const Mat32 mat32_1(std::array<lml::Vec3, 2> {lml::Vec3(-6.0f, -3.0f, -5.0f), lml::Vec3(-2.0f, -4.0f, -1.0f)});
           
            const auto mat23_2 = mat23_1;
            const auto mat32_2 = Mat32(std::array<lml::Vec3, 2> {lml::Vec3(-6.0f, -3.0f, -5.0f), lml::Vec3(-2.0f, -4.0f, -1.0f)});
            Assert::AreEqual(mat23_2, mat23_1);
            Assert::AreNotEqual(mat23_2 == mat23_1, mat23_2 != mat23_1);
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(2.0f, 4.0f), lml::Vec2(6.0f, 8.0f), lml::Vec2(10.0f, 12.0f)}), mat23_1 + mat23_2);
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(0.0f), lml::Vec2(0.0f), lml::Vec2(0.0f)}), mat23_2 - mat23_1);
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(3.0f, 6.0f), lml::Vec2(9.0f, 12.0f), lml::Vec2(15.0f, 18.0f)}), (mat23_1 * 3));
            Assert::AreEqual(lml::Matrix<float, 3, 3>(std::array<lml::Vec3, 3> {lml::Vec3(-10.0f, -11.0f, -7.0f), lml::Vec3(-26.0f, -25.0f, -19.0f), lml::Vec3(-42.0f, -39.0f, -31.0f)}), (mat32_1 * mat23_1));
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(3.0f, 6.0f), lml::Vec2(9.0f, 12.0f), lml::Vec2(15.0f, 18.0f)}), (3 * mat23_1));
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(1.0f/3.0f, 2.0f / 3.0f), lml::Vec2(3.0f / 3.0f, 4.0f / 3.0f), lml::Vec2(5.0f / 3.0f, 6.0f / 3.0f)}), mat23_1 / 3);
        }

        TEST_METHOD(UnaryOperators)
        {
            const Mat23 mat23_1(std::array<lml::Vec2, 3> {lml::Vec2(1.0f, 2.0f), lml::Vec2(3.0f, 4.0f), lml::Vec2(5.0f, 6.0f)});
            const Mat32 mat32_1(std::array<lml::Vec3, 2> {lml::Vec3(-6.0f, -3.0f, -5.0f), lml::Vec3(-2.0f, -4.0f, -1.0f)});;
            auto mat23_2 = mat23_1;
            mat23_2 += mat23_1;
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(2.0f, 4.0f), lml::Vec2(6.0f, 8.0f), lml::Vec2(10.0f, 12.0f)}), mat23_2);

            mat23_2 = mat23_1;
            mat23_2 -= mat23_1;
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(0.0f), lml::Vec2(0.0f), lml::Vec2(0.0f)}), mat23_2);

            mat23_2 = mat23_1;
            mat23_2 *= 3;
            Assert::AreEqual(Mat23(std::array<lml::Vec2, 3> {lml::Vec2(3.0f, 6.0f), lml::Vec2(9.0f, 12.0f), lml::Vec2(15.0f, 18.0f)}), mat23_2);

            auto mat32_2 = mat32_1;;
            mat32_2 /= 3;
            Assert::AreEqual(Mat32(std::array<lml::Vec3, 2> {lml::Vec3(-6.0f/3.0f, -3.0f / 3.0f, -5.0f / 3.0f), lml::Vec3(-2.0f / 3.0f, -4.0f / 3.0f, -1.0f / 3.0f)}), mat32_2);
        }

        TEST_METHOD(Functions)
        {
           
        }

    };
}